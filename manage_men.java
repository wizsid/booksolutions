import java.io.*;
import java.lang.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

class manage_men
{
static int d,m,due,staarray=0,bookarray=0,memberarray=0,memberpos=0,gbookarray=0,telearray=0,addarray=0,namearr=0;
String text;
InputStreamReader reader=new InputStreamReader(System.in);
BufferedReader input= new BufferedReader(reader);
int date[]={31,28,31,30,31,30,31,31,30,31,30,31};
int members[]=new int[200];
String status[]=new String[200];
String books[]=new String[200];
String names[]=new String[200];
String telefone[]=new String[200];
String GlobalBooks[]=new String[200];
String address[]=new String[200];
public void duecal(int day,int month)throws IOException
{
if((day+7)>date[month-1])
{
int datetemp=(date[month-1]-day);
due=7-datetemp;
}
else
{
due=day+7;
}
}

public int membership()throws IOException
{
int buffer=-1;
do
{
System.out.println("Enter your membership number");
int mem=Integer.parseInt(input.readLine());
for (int i=0;i<200;i++)
{
if (members[i]==mem)
{
memberpos=i;
buffer=1;
break;
}
else
{
buffer=0;
}
}
if (buffer==0)
{
System.out.println("Wrong code entered.");
}
}while (buffer==0);
return memberpos;
}

public void arrayval()throws IOException
{
File file = new File("membercodes.txt");
BufferedReader br  = new BufferedReader(new FileReader(file));
for(int i=0;i<200;i++)
{String sta=br.readLine();
if (sta==null)
{
memberarray=i;
break;
}
else
{
members[i]=Integer.parseInt(sta);
}
}
File file5 = new File("names.txt");
BufferedReader br4  = new BufferedReader(new FileReader(file5));
for(int i=0;i<200;i++)
{
String sta=br4.readLine();
if (sta==null)
{
namearr=i;
break;
}
else
{
names[i]=sta;
}
}
File status1 = new File("status.txt");
BufferedReader in  = new BufferedReader(new FileReader(status1));
for(int i=0;i<200;i++)
{
String sta=in.readLine();
if (sta==null)
{
staarray=i;
break;
}
else
{
status[i]=sta;
}
}

File file1 = new File("book.txt");
BufferedReader inp  = new BufferedReader(new FileReader(file1));
for(int i=0;i<200;i++)
{
String sta=inp.readLine();
if (sta==null)
{
bookarray=i;
break;
}
else
{
books[i]=sta;
}
}
File file2 = new File("gbook.txt");
BufferedReader br1  = new BufferedReader(new FileReader(file2));
for(int i=0;i<200;i++)
{String sta=br1.readLine();
if (sta==null)
{
gbookarray=i;
break;
}
else
{
GlobalBooks[i]=sta;
}
}
File file3 = new File("address.txt");
BufferedReader br2  = new BufferedReader(new FileReader(file3));
for(int i=0;i<200;i++)
{String sta=br2.readLine();
if (sta==null)
{
addarray=i;
break;
}
else
{
address[i]=sta;
}
}
File file4 = new File("telefone.txt");
BufferedReader br3  = new BufferedReader(new FileReader(file4));
for(int i=0;i<200;i++)
{String sta=br3.readLine();
if (sta==null)
{
telearray=i;
break;
}
else
{
telefone[i]=sta;
}
}
}
public void dateassign()throws IOException
{
boolean flag=false;
int dd,mm,yy;
System.out.println("Enter the present date in the format dd-mm");
do
{
text=input.readLine();
int x=text.indexOf('-');
dd= Integer.parseInt(text.substring(0,x));
mm=Integer.parseInt(text.substring(x+1));
if(dd>0 && dd<32 && mm>0 && mm<13)
{
d=dd;
m=mm;
break;
}
else
{
System.out.println("Incorrect date entered. Re-enter the date please:-");
flag=true;
}
}while(flag=true);
}
public int printmenu()throws IOException
{
int x=0;
System.out.println("   Enter the corresponding number for the action you want to perform");
System.out.println("1-Borrow a book");
System.out.println("2-Return a book");
System.out.println("3-Check availability of books");
System.out.println("4-Forgot your membership number?");
System.out.println("5-Update your information");
System.out.println("6-Add a new book");
System.out.println("7-Add a new member");
System.out.println("8-Exit");
text=input.readLine();
try
{
x=Integer.parseInt(text);
}
catch (NumberFormatException e)
{

}
if (x>0 && x<9)
return x;
else
{
System.out.println("Incorrect code entered. Re-enter the code please:-");
text=input.readLine();
x=Integer.parseInt(text);
return x;
}
}
public void issue (int mem)throws IOException
{
System.out.print("\u000C");
int buffer=0;
do
{
System.out.println("Enter the book name");
String temp=input.readLine();
for (int i=0;i<=bookarray;i++)
{
try
{
if (books[i].equalsIgnoreCase(temp))
{
buffer=1;
String chk= "none";
if (status[mem].equalsIgnoreCase(chk))
{
status [mem]=""+due;
books[i]="none";
buffer=2;
}
else
System.out.println("You have already borrowed a book");
break;
}
}

catch (NullPointerException e)
{
System.out.println("You have entered a book which is not available at the library");
}
}
}while (buffer==0);


if (buffer==2)
{
System.out.println("Your Issue has been recorded");
if (due>d && m==12)
{
System.out.println("The due date is "+ due+"-"+"01" );
}
else if (due>d)
{
System.out.println("The due date is "+ due+"-"+ m );
}
else if (m==12)
{
System.out.println("The due date is "+ due+"-"+"01" );
}
else
{
System.out.println("The due date is "+ due+"-"+(m+1) );
}
}
}
public void returnbook(int mem)throws IOException
{
System.out.print("\u000C");
System.out.println("Enter the book you are returning:");
String temp=input.readLine();
for(int i=0;i<bookarray;i++)
{
if(books[i].equalsIgnoreCase("none"));
{
books[i]=temp;
break;
}
}
int f=Integer.parseInt(status[mem]);
status[mem]="none";
if(f<d)
{
int fi=(d-f)*10;
System.out.println("Your fine amount is Rs."+ fi );
}
else if (d<7)
{
if (f>(date[m-2]-7))
{
int fin=(d+(date[m-2]-f));
System.out.println(fin);
int fi=fin*10;
System.out.println("Your fine amount is "+ fi);
}
}
else
System.out.println("The book has been returned");
}

public void rewrite()throws IOException
{
String sta;
FileWriter f0 = new FileWriter("books.txt"); 
for (int i=0;i<bookarray;i++)
{
sta = books[i]; 
     f0.write(sta+ "\n");
    }
f0.close(); 
FileWriter f1 = new FileWriter("status.txt");
for (int i=0;i<staarray;i++)
{
sta = status[i]; 
     f1.write(sta+ "\n");
    }

f1.close(); 
FileWriter f2 = new FileWriter("membercodes.txt");
for (int i=0;i<memberarray;i++)
{
sta = "" +members[i]; 
     f2.write(sta+ "\n");
    }

f2.close(); 
FileWriter f3 = new FileWriter("names.txt"); 
for (int i=0;i<namearr;i++)
{
sta = names[i]; 
     f3.write(sta+ "\n");
    }
f3.close(); 
FileWriter f4 = new FileWriter("telefone.txt"); 
for (int i=0;i<telearray;i++)
{
sta = telefone[i]; 
     f4.write(sta+ "\n");
    }
f4.close(); 
FileWriter f5 = new FileWriter("address.txt"); 
for (int i=0;i<addarray;i++)
{
sta = address[i]; 
     f5.write(sta+ "\n");
    }
f5.close(); 
FileWriter f6 = new FileWriter("gbook.txt"); 
for (int i=0;i<gbookarray;i++)
{
sta = GlobalBooks[i]; 
     f6.write(sta+ "\n");
    }
f6.close(); 
}

public void AddMember()throws IOException
{
System.out.print("\u000C");
if (memberarray>=200)
{
System.out.println("Sorry try again later");
}
else
{
System.out.println("Enter your name");
names[memberarray]=input.readLine();
System.out.println("Enter your telephone number");
telefone[memberarray]=input.readLine();
namearr++;
telearray++;
System.out.println("Enter your address");
address[memberarray]=input.readLine();
members[memberarray]=memberarray+1;
System.out.println(members[memberarray]);
addarray++;
System.out.println("                         Your membership is confirmed!!!!! :D");
memberarray++;
System.out.println("Your membership number is "+memberarray);
}
}
public void AddBook()throws IOException
{
System.out.print("\u000C");
System.out.println("Please enter the name of the book to be added");
books[bookarray]=input.readLine();
bookarray++;
GlobalBooks[bookarray]=books[bookarray];
gbookarray++;
System.out.println("The book has been added");
}

public void Enquiry()throws IOException
{
System.out.println("Enter the corresponding number for the action you want to perform");
System.out.println("1-A complete set of books");
System.out.println("2-Check wether the book is available");
int buffer=1,i;
switch(Integer.parseInt(input.readLine()))
{
case 1 :
{
System.out.println("Here are the set of books the library offers: ");
for(i=0;i<gbookarray;i++)
{
System.out.println(GlobalBooks[i]);
}
break;
}
case 2 :
{
System.out.println("Enter the book you are looking for");
String booke=input.readLine();
for( i=0;i<bookarray;i++)
{
if (booke.equalsIgnoreCase(GlobalBooks[i]))
{
buffer =2;
break;
}
}
if (buffer==1)
{
System.out.println("The book is not available at the library");
}
if(books[i].equalsIgnoreCase("none"))
{
buffer=3;
}

switch (buffer)
{
case 2 :
{
System.out.println("The book is available for issue in the library.");
break;
}
case 3 :
{
System.out.println("The book is currently issued to another member. Please try again later.");
break;
}
default :
{
System.out.println("Wrong code entered");
}
}
}
}
}
public void Forgotten()throws IOException
{
System.out.print("\u000C");
System.out.println("Enter your member name");
String fname=input.readLine();
System.out.println("Enter your telephone number");
String ftele=input.readLine();
int buffer=0;
int i=0;
for(i=0;i<=199;i++)
{
if(fname.equalsIgnoreCase(names[i]) && ftele.equals(telefone[i]))
{
System.out.println("Your membership number is "+members[i]);
buffer=1;
break;
}
else
{
buffer=2;
}
}
if(buffer==2)
{
System.out.println("You are not registered with us");
}
}
public void Updater()throws IOException
{
System.out.print("\u000C");
System.out.println("Enter your member name");
String fname=input.readLine();
System.out.println("Enter your telephone number");
String ftele=input.readLine();
boolean buffer=true;
int i;
for(i=0;i<=199;i++)
{
if(fname.equalsIgnoreCase(names[i]) && ftele.equals(telefone[i]))
{
buffer=false;
break;
}
}
if(buffer)
{
System.out.println("You are not registered with us");
}
else
{
System.out.println("Enter the corresponding number for the action you want to perform");
System.out.println("1-Modify your telephone number");
System.out.println("2-Modify your address");
switch(Integer.parseInt(input.readLine()))
{
case 1 :
{
System.out.println("Enter your new Telephone number");
telefone[i]=input.readLine();
System.out.println("The information has been updated");
break;
}
case 2 :
{
System.out.println("Enter your new Address");
address[i]=input.readLine();
System.out.println("The information has been updated");
break;
}
default :
{
System.out.println("Wrong code entered");
}
}
}
}
public static void main()throws IOException
{
InputStreamReader r=new InputStreamReader(System.in);
BufferedReader i= new BufferedReader(r);
System.out.println("                              Welcome to the Library");
System.out.println("");
manage_men obj=new manage_men();
obj.arrayval();

obj.dateassign();
obj.duecal(d,m);
int code;
char perm='y';
while(perm=='y' || perm=='Y')
{
code=obj.printmenu();
switch (code)
{
case 1 :
{
System.out.print('\u000c');
int mem=obj.membership();
obj.issue(mem);
obj.rewrite();
break;
}
case 2 :
{
System.out.print('\u000c');
int mem=obj.membership();
obj.returnbook(mem);
obj.rewrite();
break;
}
case 3 :
{
System.out.print('\u000c');
obj.Enquiry();
break;
}
case 4 :
{
System.out.print('\u000c');
obj.Forgotten();
obj.rewrite();
break;
}
case 5 :
{
System.out.print('\u000c');
obj.Updater();
obj.rewrite();
break;
}
case 6 :
{
System.out.print('\u000c');
obj.AddBook();
obj.rewrite();
break;
}
case 7 :
{
System.out.print('\u000c');
obj.AddMember();
obj.rewrite();
break;
}
case 8 :
{
System.out.println("                    Thank you for visiting the Library. Visit again.");
System.exit(0);
}
}
System.out.println("Do you want to continue? (y/n)");
perm=i.readLine().charAt(0);
}
}
}